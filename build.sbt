val scalaVer = "2.11.8"
val akkaVersion = "2.4.11"
val logbackVersion = "1.1.3"
val scalaParserVersion = "1.0.4"
val scalaTestVersion = "2.2.4"

lazy val compileOptions = Seq(
  "-unchecked",
  "-deprecation",
  "-language:_"
)

// If you use groupID %% artifactID % revision instead of groupID % artifactID % revision (the difference is the double %% after the groupID),
// sbt will add your project’s Scala version to the artifact name. This is just a shortcut.
lazy val commonDependencies = Seq(
  "com.typesafe.akka" %% "akka-actor" % akkaVersion,
  "com.typesafe.akka" %% "akka-slf4j" % akkaVersion,
  "com.typesafe.akka" %% "akka-http-core" % akkaVersion,
  "com.typesafe.akka" %% "akka-stream" % akkaVersion,
  "com.typesafe.akka" %% "akka-http-experimental" % akkaVersion,
  "com.typesafe.akka" %% "akka-persistence" % akkaVersion,
  "ch.qos.logback" % "logback-classic" % logbackVersion,
  "org.scala-lang.modules" %% "scala-parser-combinators" % scalaParserVersion,
  "com.typesafe.akka" %% "akka-testkit" % akkaVersion,
  "com.typesafe.akka" %% "akka-http-testkit" % akkaVersion,
  "com.typesafe.akka" %% "akka-http-testkit-experimental" % "2.0",
  "org.scalatest" %% "scalatest" % scalaTestVersion,
  "org.iq80.leveldb" % "leveldb"  % "0.7",
  "org.fusesource.leveldbjni" % "leveldbjni-all" % "1.8"
)

name := "hong_poc"
organization := "com.starbucks"
version := "1.0.0"
scalaVersion := scalaVer
scalacOptions ++= compileOptions
libraryDependencies ++= commonDependencies