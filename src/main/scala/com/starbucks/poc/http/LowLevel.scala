package com.starbucks.poc.http

import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.http.scaladsl.model.{HttpEntity, HttpRequest, HttpResponse, Uri}
import akka.stream.ActorMaterializer
import akka.stream.scaladsl.Sink
import akka.http.scaladsl.model.HttpMethods._

import scala.concurrent.Future

/**
  * Project: hong_poc
  * Author: honzhang on 10/13/2016.
  */
object LowLevel extends App {
  implicit val system = ActorSystem()

  implicit val materializer = ActorMaterializer()
  implicit val ec = system.dispatcher

  val serverSource = Http().bind(interface = "localhost", port=8888)

  val requestHandler : HttpRequest => HttpResponse = {
    case HttpRequest(GET, Uri.Path("/"), _, _, _) =>
      HttpResponse(entity = HttpEntity("Hello World Low Level!"))
    case _ =>
      HttpResponse(404, entity = "Unknown resource!")
  }

  val bindingFuture: Future[Http.ServerBinding] =
    serverSource.to(Sink.foreach{ conn =>
      println("Accepted new connection from " + conn.remoteAddress)

      conn handleWithSyncHandler requestHandler
    }).run()

  println("Server is running...")
  scala.io.StdIn.readLine()

  bindingFuture.flatMap(_.unbind()).onComplete(_=>system.terminate())
}
