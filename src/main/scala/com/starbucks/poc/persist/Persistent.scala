package com.starbucks.poc.persist

import akka.actor.{ActorSystem, Props}

/**
  * Project: hong_poc
  * Author: honzhang on 10/11/2016.
  */
object Persistent extends App {

  import Counter._

  val system = ActorSystem("persistent-actors")
  val counter = system.actorOf(Props[Counter], "counter")

  counter ! Cmd(Increment(3))
  counter ! Cmd(Increment(5))
  counter ! "print"

  Thread.sleep(1000)
  system.terminate()
}
