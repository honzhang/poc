import akka.http.scaladsl.testkit.ScalatestRouteTest
import org.scalatest.{FlatSpec, MustMatchers}

/**
  * Project: hong_poc
  * Author: honzhang on 10/13/2016.
  */
class LowLevelServiceTest extends FlatSpec
  with MustMatchers
  with ScalatestRouteTest {

  // ~> must take 2 arguments, 1 is route, can't se this way to test low level api
  "The server" should " say hello" in {
    Get("/") ~> check {
      status must equal(200)
      val res = responseAs[String]
      res must equal("Hello World Low Level!")
    }
  }
}
